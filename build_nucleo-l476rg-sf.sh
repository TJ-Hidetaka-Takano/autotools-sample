#!/bin/sh

autoreconf -vfi

CC="arm-none-eabi-gcc"
CXX="arm-none-eabi-g++"
LD="arm-none-eabi-ld"
AR="arm-none-eabi-ar"

NUTTX_EXPORT_PATH=`test -d "${0%/*}/nuttx-export" && cd ${0%/*}/nuttx-export ; pwd`

CPPFLAGS="-isystem ${NUTTX_EXPORT_PATH}/include -isystem ${NUTTX_EXPORT_PATH}/include/cxx" \
CFLAGS="-g -O2 -Wall -Wstrict-prototypes -Wshadow -Wundef -fno-builtin -mcpu=cortex-m4 -mthumb -mfloat-abi=soft" \
CXXFLAGS="-g -O2 -Wall -Wshadow -Wundef -fno-builtin -fno-exceptions -fcheck-new -fno-rtti -mcpu=cortex-m4 -mthumb -mfloat-abi=soft" \
LDFLAGS="--entry=__start -nostartfiles -nodefaultlibs -L${NUTTX_EXPORT_PATH}/libs -T${NUTTX_EXPORT_PATH}/build/l476rg.ld" \
LIBS="-Wl,--start-group -lapps -lnuttx -lgcc -Wl,--end-group" \
./configure --host=arm-none-eabi

make

