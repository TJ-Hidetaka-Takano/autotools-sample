#include <config.h>
#include <stdio.h>

int test(int i)
{

#ifdef USE_HOGE
  printf("enabled hoge! (--enable-hoge)\n");
#else
  printf("disabled hoge! (default/--disable-hoge)!\n");
#endif

  return i;
}
